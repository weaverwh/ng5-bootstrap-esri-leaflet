import { Component } from '@angular/core';
import { tileLayer, latLng } from 'leaflet';
import * as esri from "esri-leaflet";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  options = {
  	layers: [
  		tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
  	],
  	zoom: 5,
  	center: latLng(46.879966, -121.726909)
  };
}
